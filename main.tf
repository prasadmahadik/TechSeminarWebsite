provider "aws" {
    region = "us-west-2"
    profile = "developer"
}


resource "aws_s3_bucket" "tech_seminar_bucket" {
    bucket = "tech-seminar-terraform1"
    force_destroy = true
    acl    = "public-read"
    # website {
    #     index_document = "index.html"
    #     error_document = "index.html" 
    # }
}

resource "aws_s3_bucket_object" "index_html" {
    bucket = aws_s3_bucket.tech_seminar_bucket.bucket
    key = "index.html"
    content_type = "text/html"
    source = "./index.html"
    # acl    = "public-read"
    # etag   = filemd5("./index.html")
}

resource "aws_s3_bucket_object" "js_test_js" {
    bucket = aws_s3_bucket.tech_seminar_bucket.bucket
    key = "js/test.js"
    content_type = "application/json"
    source = "./js/test.js"
    # acl    = "public-read"
    # etag   = filemd5("./js/test.js")
}

resource "aws_s3_bucket_object" "css_test_css" {
    bucket = aws_s3_bucket.tech_seminar_bucket.bucket
    key = "css/test.css"
    content_type = "text/css"   
    source = "./css/test.css"
    # acl    = "public-read"
    # etag   = filemd5("./css/test.css")
}





























# module "testmodule" {
#     source = "./modules/testmodule"
#     bucketname = aws_s3_bucket.tech_seminar_bucket.bucket
#     keyname = "index.html"
#     contenttype = "text/html"
#     sourcename = "./index.html"
#     # acl    = "public-read"
#     # etag   = filemd5("./index.html")
# }

# module "testmodule2" {
#     source = "./modules/testmodule"
#     bucketname = aws_s3_bucket.tech_seminar_bucket.bucket
#     keyname = "js/test.js"
#     contenttype = "application/json"
#     sourcename = "./js/test.js"
#     # acl    = "public-read"
#     # etag   = filemd5("./js/test.js")
# }

# module "testmodule3" {
#     source = "./modules/testmodule"
#     bucketname = aws_s3_bucket.tech_seminar_bucket.bucket
#     keyname = "css/test.css"
#     contenttype = "text/css"
#     sourcename = "./css/test.css"
#     # acl    = "public-read"
#     # etag   = filemd5("./css/test.css")
# }


































































