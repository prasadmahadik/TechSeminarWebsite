function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var ms = today.getMilliseconds()
    m = checkTime(m);
    s = checkTime(s);
    console.log(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
  }
  function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
  }
  
  var quotes = [
    "The two most powerful warriors are patience and time. - Leo Tolstoy, War and Peace.",
    "Better three hours too soon than a minute too late. - William Shakespeare.",
    "The key is in not spending time, but in investing it. - Stephen R. Covey.",
    "The trouble is, you think you have time. - Jack Kornfield",
    "Time takes it all, whether you want it to or not. - Stephen King, The Green Mile"
  ];

  function setQuote(){
    var data = 0;
    document.getElementById("quote").innerHTML = quotes[data]
    setInterval(() => { document.getElementById("quote").innerHTML = quotes[data]; if(data < 4)data++; else data = 0;  }, 5000);
    
  }

  