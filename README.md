Thanks for visiting !

Let's setup the things :
- Run `npm i` in the directory (after running this command, you can see package.lock.json).
- Run `http-server` command to run the website locally. 
- As the output of the http-server command, you can see different urls.
- You can use any url for visiting website

In case of any issues, please feel free to reach to Abdul : abdul.thag@nvent.com or Prasad : prasad.mahadik@nvent.com