resource "aws_s3_bucket_object" "example" {
    bucket = var.bucketname
    key = var.keyname
    content_type = var.contenttype
    source = var.source
    acl    = var.acl
    etag   = var.etag
}